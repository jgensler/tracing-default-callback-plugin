# (c) 2012-2014, Michael DeHaan <michael.dehaan@gmail.com>
# (c) 2017 Ansible Project
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

# Make coding more python3-ish
from __future__ import (absolute_import, division, print_function)

from ansible.constants import BOOL_TRUE
from opentelemetry.trace import span
__metaclass__ = type

DOCUMENTATION = '''
    name: minimal
    type: stdout
    short_description: minimal Ansible screen output
    version_added: historical
    description:
        - This is the default output callback used by the ansible command (ad-hoc)
'''
from ansible import constants as C
from ansible.plugins.callback.default import CallbackModule as DefaultCallbackModule
from ansible.playbook.task import Task
from ansible.playbook import Playbook
from opentelemetry import trace
from opentelemetry.sdk.trace import Tracer, Span, TracerProvider
from opentelemetry.sdk.trace.export import (
    BatchSpanProcessor,
    ConsoleSpanExporter,
)
from opentelemetry.sdk.resources import Resource
from opentelemetry.exporter.otlp.proto.grpc.trace_exporter import OTLPSpanExporter


class CallbackModule(DefaultCallbackModule):
    '''
    This is the default callback interface, which simply prints messages
    to stdout when new callback events are received.
    '''

    CALLBACK_VERSION = 2.0
    CALLBACK_TYPE = 'stdout'
    CALLBACK_NAME = 'minimal'

    tracer: Tracer
    playbook_span: Span = None
    runner_span: Span = None

    def __init__(self):
        super(CallbackModule, self).__init__()
        resource = Resource(attributes={
            "service.name": "callback_module"
        })
        trace.set_tracer_provider(TracerProvider(resource=resource))
        # trace.set_tracer_provider(TracerProvider())
        otlp_exporter = OTLPSpanExporter(endpoint="http://localhost:4317", insecure=True)
        span_processor = BatchSpanProcessor(otlp_exporter)
        # trace.get_tracer_provider().add_span_processor(
        #     BatchSpanProcessor(ConsoleSpanExporter())
        # )
        trace.get_tracer_provider().add_span_processor(span_processor)
        self.tracer = trace.get_tracer(__name__)

    #
    # Playbook
    #
    def _playbook_span_start(self):
        # TOOD(jgensler) actually figure out how multiple playbooks works
        if self.playbook_span:
            if self.playbook_span.is_recording():
                self._playbook_span_end()
        self.playbook_span = self.tracer.start_as_current_span("playbook")
        self.playbook_span.__enter__()

    def _playbook_span_end(self):
        self.playbook_span.__exit__(None, None, None)

    def v2_playbook_on_start(self, playbook: Playbook):
        self._playbook_span_start()
        return super().v2_playbook_on_start(playbook)

    def v2_playbook_on_task_start(self, task: Task, is_conditional: bool):
        return super().v2_playbook_on_task_start(task, is_conditional)

    def v2_playbook_on_cleanup_task_start(self, task: Task):
        print("XXX")
        print(task.get_name())
        return super().v2_playbook_on_cleanup_task_start(task)
    
    def v2_playbook_on_handler_task_start(self, task: Task):
        print("XXX")
        print(task.get_name())
        return super().v2_playbook_on_handler_task_start(task)

    #
    # Runner
    #
    def _runner_span_start(self, task: Task):
        print("RUNNER SPAN START")
        self.runner_span = self.tracer.start_as_current_span(task.get_name())
        self.runner_span.__enter__()
    
    def _runner_span_end(self):
        print("RUNNER SPAN END")
        self.runner_span.__exit__(None, None, None)

    def v2_runner_on_start(self, host, task: Task):
        print("RRR_start")
        self._runner_span_start(task)
        return super().v2_runner_on_start(host, task)

    def v2_runner_on_ok(self, result):
        print("RRR_ok")
        self._runner_span_end()
        return super().v2_runner_on_ok(result)

    def v2_runner_on_failed(self, result, ignore_errors):
        print("RRR_failed")
        self._runner_span_end()
        return super().v2_runner_on_failed(result, ignore_errors=ignore_errors)

    def v2_runner_on_skipped(self, result):
        print("RRR_result")
        self._runner_span_end()
        return super().v2_runner_on_skipped(result)
    
    def v2_runner_on_unreachable(self, result):
        print("RRR_unreach")
        self._runner_span_end()
        return super().v2_runner_on_unreachable(result)
