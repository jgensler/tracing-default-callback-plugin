# tracing "default" callback module

## how to test locally

1. `docker-compose up`

2. `ansible-playbook main.yml -i inventory.toml --connection=local`

## host to use with existing playbook

1. update `ansible.cfg`

```
callback_plugins=<path-to-this-repo>/plugins/callback
stdout_callback=myminimal
```

2. `docker-compose up`

3. `ansible-playbook ... whatever-you-usually-put ...`

## notes

docker-compose.yml pulled from 
